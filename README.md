# Akoziland Linux

*(most features mentioned here are currently in development, DO NOT install on real hardware unless you know what you're doing)*

### Why?

Akoziland Linux is a Linux distribution with the goal of remaining minimal in it's required defaults, while giving the user choice over what to include is their _koziland_. It's got three primary reasons for existing:

- ### If it doesn't need to be persistant, it's not persistant
    If a live usb can boot without anything persistant, why not take this concept to a whole distro! Not to worry, your home directory & user account are persistant. And if keeping all your configs in `$HOME` doesn't do it for you, then you can create *post install steps* to insert your own system configs into the image, with little effort.

- ### Not everyone finds the same things to be essential
    You can use the *post install steps* mentioned before to remove what you don't need, but the base image will aim at including *only* essentials, the rest will be offered during install (or available as a part of the desktop image or a communtiy made image).

- ### Stability of an 'immutable' distro
    Similarly to an immutable distro, changes are limited (as mentioned before, changes are exclusive to saving user accounts and your home directory), eventually it should be possible to store things in a persistent partition that stores only what is added by the user and gets symlinked to the corresponding locations in the primary filesystem. Due to the approach used, changes by most traditional package managers aren't persistant across reboots, but if you choose to include a traditional package manager, it can still be used for temporary installs.

### Where can I get it?
Image builds are automated to stay up-to-date with security updates, you can get builds of it here: https://github.com/Akoziland/Image-Build-Automation/actions, if you'd like, you can also build it yourself on Debian, or any Debian based distro, but be warned that you may need to do so quite frequently to keep your system secure.

### How can I build this?
just run: ```curl -s https://gitlab.com/akoziland/akoziland-linux/-/raw/main/build.sh | sudo /bin/bash -s https://gitlab.com/akoziland/akoziland-linux```

### Why not base on another distro?
It felt easier to achieve minimalism & incorporate this uncommon approach at immutability in an expandable form.
