#!/bin/sh

# repo root path
repo_root=$(cd $(dirname $0) && pwd)

# other useful directories
out=$repo_root/out # output directory

# prepare deps
apt update
apt install syslinux dosfstools bc binutils bison dwarves flex gcc git gnupg2 gzip libelf-dev libncurses5-dev libssl-dev make openssl pahole perl-base rsync tar xz-utils

# build linux
git clone --depth 1 https://github.com/torvalds/linux.git $out/linux
cd $out/linux
make defconfig
./scripts/config --file .config --set-str SYSTEM_TRUSTED_KEYS ""
./scripts/config --file .config --set-str SYSTEM_REVOCATION_KEYS ""
make -j$(nproc) 2>&1 | tee $out/log ||
print "Failed to build Linux :(" ||
exit
cp ./arch/x86/boot/bzImage $out

# setup initramfs
git clone --depth 1 $1 $out/akoziramfs
cd $out/akoziramfs
./build.sh ./req-scripts/create-init.sh ./req-scripts/busybox.sh
cp ./out/init.cpio $out/init.cpio

# set up bootloader
cd $out
truncate -s 50M $out/boot
mkfs -t fat $out/boot
cat > $out/syslinux.cfg <<EOF
PROMPT 0
TIMEOUT 0
DEFAULT boot

LABEL boot
    LINUX /bzImage
        INITRD /init.cpio
EOF
syslinux $out/boot

# copy linux and init.cpio to boot
mkdir mnt
mount boot mnt
cp bzImage init.cpio mnt
umount mnt
